/*************************************************************************
                           Clavier  -  description
                             -------------------
    début                : Clavier
    copyright            : (C) Clavier par Clavier
    e-mail               : Clavier
*************************************************************************/

//---------- Réalisation du module <Clavier> (fichier Clavier.cpp) -----

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Clavier.h"
#include "Config.h"

///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques
static int matriculeVoitures = 1;
static int CANAL_PROF_BP;
static int CANAL_AUTRE_BP;
static int CANAL_GB;
static int CANAL_SORTIE;
//------------------------------------------------------ Fonctions privées


//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
void Clavier()
// Initialisation : Ouverture des canaux en ecriture
// Phase moteur : appel de la procédure Menu
{
	// Ouverture des canaux
	CANAL_PROF_BP = open("CANAL_PROF_BP", O_WRONLY);
	CANAL_AUTRE_BP = open("CANAL_AUTRE_BP", O_WRONLY);
	CANAL_GB = open("CANAL_GB", O_WRONLY);
	CANAL_SORTIE = open("CANAL_SORTIE", O_WRONLY);

	for (;;) {
		Menu();
	}
} //----- fin de Clavier

static void EntreeVoiture(char code, unsigned int valeur)
// Procédure qui s'occupe d'envoyer une voiture sur le canal approprie
{
	// Creation d'une voiture
	Voiture voiture;
	voiture.dateEntree = time(NULL);
	voiture.matricule = (matriculeVoitures++)%1000;

	//Arrivee Prof a Blaise Pascal
	if(code == 'P' && valeur == 1) {
		voiture.conducteur = PROF;
		write(CANAL_PROF_BP, &voiture, sizeof(Voiture));
	}
	//Arrivee Prof a GB
	else if(code == 'P' && valeur == 2) {
		voiture.conducteur = PROF;
		write(CANAL_GB, &voiture, sizeof(Voiture));
	}
	//Arrivee Autre a Blaise Pascal
	else if(code == 'A' && valeur == 1) {
		voiture.conducteur = AUTRE;
		write(CANAL_AUTRE_BP, &voiture, sizeof(Voiture));
	}
	//Arrivee Autre a GB
	else if(code == 'A' && valeur == 2) {
		voiture.conducteur = AUTRE;
		write(CANAL_GB, &voiture, sizeof(Voiture));
	}
}

static void SortieVoiture(unsigned int place)
// Procédure qui s'occupe d'envoyer le numero de la place concernee par la sortie sur le canal CANAL_SORTIE
{
	write(CANAL_SORTIE, &place, sizeof(int));
}

static void destruction()  {
	// Fermeture des canaux
	close(CANAL_PROF_BP);
	close(CANAL_AUTRE_BP);
	close(CANAL_GB);
	close(CANAL_SORTIE);
	// Fin du processus
	exit(0);
}

void Commande(char code, unsigned int valeur)
// Appelle la procedure concernee en fonction de la commande utilisateur
{
	switch(code) {
		case 'Q' :
			destruction();
			break;

		case 'A' :
			EntreeVoiture('A', valeur);
			break;

		case 'P' :
			EntreeVoiture('P', valeur);
			break;

		case 'S' :
			SortieVoiture(valeur);
			break;

		default :
			break;
	}
} //----- fin de Commande

