/*************************************************************************
                           Clavier  -  Tache clavier
                             -------------------
    début                : Clavier
    copyright            : (C) Clavier par B3430
    e-mail               : Clavier
*************************************************************************/

//---------- Interface du module <Clavier> (fichier Clavier.h) ---------
#if ! defined ( Clavier_H )
#define Clavier_H

//------------------------------------------------------------------------
// Rôle du module <Clavier>
//
//
//------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////  INCLUDE
//--------------------------------------------------- Interfaces utilisées
#include "/shares/public/tp/tp-multitache/Menu.h"
#include <stdlib.h>
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
void Clavier();
// Mode d'emploi :
// Procedure de la tache Clavier
// Contrat :
// Aucun

void Commande(char code, unsigned int valeur);
// Mode d'emploi :
// Appelle la procedure concernee en fonction de la commande utilisateur
// Contrat :
// Codes possibles : P, A, S, Q. valeurs possibles : 0, 1, 2
// Toute autre code/valeur n'aboutira a rien

#endif // Clavier_H

