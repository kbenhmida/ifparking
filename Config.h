/*************************************************************************
                           Config
                             -------------------
    début                : Config
    copyright            : (C) Config par Config
    e-mail               : Config
*************************************************************************/

//---------- Interface du module <Config> (fichier Config.h) ---------
#if ! defined ( Config_H )
#define Config_H

//------------------------------------------------------------------------
// Rôle du module <Config>
//
//
//------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////  INCLUDE
//--------------------------------------------------- Interfaces utilisées
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include "/shares/public/tp/tp-multitache/Outils.h"
using namespace std;
//------------------------------------------------------------- Constantes
#define TERMINAL XTERM

#define TEMPO 1
#define KEY ftok("parking", 3)
#define NB_PLACES_PARKING 8
#define NB_PORTES_ENTREE 3
#define PERMISSIONS (SHM_R|SHM_W)
#define NB_SEMAPHORES 4
//------------------------------------------------------------------ Types
typedef struct Voiture {
	TypeUsager conducteur;
	int matricule;
	time_t dateEntree;
}Voiture;

typedef struct VoituresGarees {
	Voiture tab[NB_PLACES_PARKING];
	unsigned int nbPlaces; // Places disponibles
	Voiture requetes[NB_PORTES_ENTREE];
}VoituresGarees;

typedef struct PortePrio {
    Voiture voiture;
    int numPorte;
}PortePrio;

enum semType { SEM_P1, SEM_P2, SEM_P3, SEM_SHARED_MEM };
//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

void memStart(int sharedMemId, int semTabId, VoituresGarees** voituresGarees);
// Mode d'emploi :
// Attachement a la memoire partagee (avec exclusion mutuelle)
// sharedMemId : Id de la memoire partagee en question
// semTabId : Id du semaphore d'exclusion mutuelle
// voituresGarees : structure qui recupere le contenu de la memoire partagee
// Contrat :
// Aucune verification des parametres n'est effectuee

void memStop(int semTabId, VoituresGarees** voituresGarees);
// Mode d'emploi :
// Detachement de la memoire partagee
// Contrat :
// semTabId : Id du semaphore d'exclusion mutuelle
// voituresGarees : structure ayant recupere le contenu de la memoire partagee

void setHandler(int numSignal, void (*fonction) (int));
// Mode d'emploi :
// Procedure qui assigne un handler (fonction) a un signal
// numSignal : signal concerne
// fonction : fonction qui sera appelee lors de la reception de signal.
// Contrat :
// Aucune verification des parametres n'est effectuee. L'utilisation de
// parametres non valides aura un comportement non attendu

#endif // Config_H

