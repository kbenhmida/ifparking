/*************************************************************************
                           Sorti
                             -------------------
    début                : Sortie
    copyright            : (C) Sortie par Sortie
    e-mail               : Sortie
*************************************************************************/

//---------- Interface de la tache <Sortie> (fichier Sortie.h) ---------
#if ! defined ( Sortie_H )
#define Sortie_H

//------------------------------------------------------------------------
// Rôle du module <Sortie>
//
//
//------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////  INCLUDE
//--------------------------------------------------- Interfaces utilisées
#include <stdlib.h>
#include "/shares/public/tp/tp-multitache/Outils.h"
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
void Sortie(int memId, int semId);
// Mode d'emploi :
// Procedure de la tache Sortie
// Contrat :
// memId : Id de la memoire partagee, semId : Id du semaphore mutex

#endif // Sortie_H

