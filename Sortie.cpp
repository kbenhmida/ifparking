/*************************************************************************
                           Sortie
                             -------------------
    début                : Sortie
    copyright            : (C) Sortie par Sortie
    e-mail               : Sortie
*************************************************************************/

//---------- Réalisation de la tache <Sortie> (fichier Sortie.cpp) -----

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
//------------------------------------------------------ Include personnel
#include "Sortie.h"
#include "Config.h"

///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques
static int CANAL_SORTIE;
static int sharedMemId;
static int semTabId;

static vector<pid_t> voituriers;
static unsigned int place;
static VoituresGarees * voituresGarees;
//------------------------------------------------------ Fonctions privées


//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
static int getPriorite() {
	Voiture requetes[NB_PORTES_ENTREE];

	// On recupere le tableau des requetes de la memoire partagee
	memStart(sharedMemId, semTabId, &voituresGarees);
	copy(voituresGarees->requetes, voituresGarees->requetes+NB_PORTES_ENTREE, requetes);
	memStop(semTabId, &voituresGarees);

	// tableau a taille variable qui accueillera uniquement
	// les portes ayant des requetes (taille <=3)
	vector<PortePrio> entreesEnAttente;

	for (int i = 0; i < NB_PORTES_ENTREE; i++) {
		if (requetes[i].conducteur != AUCUN) {
			entreesEnAttente.push_back({requetes[i], i+1});
		}
	}

	// 1 seule porte en attente
	if (entreesEnAttente.size() == 1) {
		return entreesEnAttente[0].numPorte;
	}
	// 2 portes en attente
	else if (entreesEnAttente.size() == 2) {
		// 2 profs ou 2 autres
		if (entreesEnAttente[0].voiture.conducteur == entreesEnAttente[1].voiture.conducteur) {
			if (min(entreesEnAttente[0].voiture.dateEntree, entreesEnAttente[1].voiture.dateEntree) == entreesEnAttente[0].voiture.dateEntree) {
				return entreesEnAttente[0].numPorte;
			}
			else {
				return entreesEnAttente[1].numPorte;
			}
		}
		else {
			if (min(entreesEnAttente[0].voiture.conducteur, entreesEnAttente[1].voiture.conducteur) ==  entreesEnAttente[0].voiture.conducteur) {
				return entreesEnAttente[0].numPorte;
			}
			else {
				return entreesEnAttente[1].numPorte;
			}
		}
	}
	// 3 portes en attente
	else if (entreesEnAttente.size() == 3) {
		if(entreesEnAttente[2].voiture.conducteur == PROF) {
			if (min(entreesEnAttente[0].voiture.dateEntree, entreesEnAttente[2].voiture.dateEntree) == entreesEnAttente[0].voiture.dateEntree) {
				return entreesEnAttente[0].numPorte;
			}
			else {
				return entreesEnAttente[2].numPorte;
			}
		}
		else {
			return entreesEnAttente[0].numPorte;
		}
	}

	return 0;
}

static void destruction(int signal);

static void finVoiturier(int signal) {
	int status;
	Voiture voiture;

	// On recupere le pid du voiturier qui vient de mourir (le pauvre)
	pid_t voiturier = waitpid(-1, &status, 0);
	unsigned int place = WEXITSTATUS(status);

	// On recupere la voiture concernee depuis le parking (memoire partagee) + maj compteur + recup nbPlaces
	//int nbPlaces;
	memStart(sharedMemId, semTabId, &voituresGarees);
	voiture = voituresGarees->tab[place-1];
	voituresGarees->tab[place-1] = {AUCUN, 0, 0};
	voituresGarees->nbPlaces++;
	//nbPlaces = voituresGarees->nbPlaces;
	memStop(semTabId, &voituresGarees);

	// On met a jour l'affichage (effacement de l'etat et affichage de la sortie)
	Effacer((TypeZone)WEXITSTATUS(status));
	AfficherSortie(voiture.conducteur, voiture.matricule, voiture.dateEntree, time(NULL));

	// On enleve le pid du voiturier concerne de la structure
	vector<pid_t>::iterator toDelete;
	for(auto it = voituriers.begin(); it != voituriers.end(); it++)  {
		if(*it == voiturier)  {
			toDelete = it;
		}
	}
	voituriers.erase(toDelete);

	// Si une place s'est liberee, on donne l'autorisation d'entree a la porte concernee
	int portePrio = getPriorite();

	if(portePrio != 0) {
		sembuf buffer = {(semType)(portePrio-1), 1, 0};
		semop(semTabId, &buffer, 1);
		memStart(sharedMemId, semTabId, &voituresGarees);
		voituresGarees->requetes[portePrio-1] = {AUCUN, 0, 0};
		memStop(semTabId, &voituresGarees);
	}
}

static void init()  {
	setHandler(SIGCHLD, finVoiturier);
	setHandler(SIGUSR2, destruction);

	CANAL_SORTIE = open("CANAL_SORTIE", O_RDONLY);
}

static void moteur() {
	// Si on lit quelquechose, on sort la voiture concernee
	if(read(CANAL_SORTIE, &place, sizeof(int)) > 0) {
		pid_t voiturierPid = SortirVoiture(place);
		voituriers.push_back(voiturierPid);
	}
}

static void destruction(int signal) {
	// On ignore SIGCHLD (pour la meme raison que dans
	// la destruction de la tache Entree
	setHandler(SIGCHLD, SIG_IGN);

	// On demande gentillement la destruction de tous les voituriers en cours
	for(auto it = voituriers.begin(); it != voituriers.end(); it++)  {
		kill(*it, SIGUSR2);
	}
	for(auto it = voituriers.begin(); it != voituriers.end(); it++)  {
		waitpid(*it, NULL, 0);
	}

	close(CANAL_SORTIE);

	exit(0);
}

void Sortie(int memId, int semId)
// Algorithme :
//
{
	sharedMemId = memId;
	semTabId = semId;

	init();
	for (;;) {
		moteur();
	}
} //----- fin de Sortie
