/*************************************************************************
                           Entree
                             -------------------
    début                : Entree
    copyright            : (C) Entree par Entree
    e-mail               : Entree
*************************************************************************/

//---------- Réalisation de la tache <Entree> (fichier Entree.cpp) -----

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <map>
#include <errno.h>
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Entree.h"
#include "Config.h"

///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques
static map<pid_t, Voiture> voituriers;
static int sharedMemId;
static int semTabId;
static VoituresGarees * voituresGarees;

static TypeBarriere typeEntree;
static int canalId;
static const char * canalName;
static short unsigned int semType;
static TypeZone zoneAffichage;
//------------------------------------------------------ Fonctions privées


//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
static void destruction(int signal);

static void finVoiturier(int signal)
// Handler du signal SIGCHLD
{
	// On recupere la place ou la voiture a ete stationnee
	int status;
	pid_t voiturier = waitpid(-1, &status, 0);
	unsigned int place = (unsigned int)WEXITSTATUS(status);

	// On cherche le voiturier qui vient de mourir dans notre structure de donnees
	// (on est surs de le trouver vu qu'il a ete cree)
	auto voiturier_it = voituriers.find(voiturier);

	// Affichage des informations de la voiture et du conducteur a la bonne place
	AfficherPlace(place ,voiturier_it->second.conducteur, voiturier_it->second.matricule, voiturier_it->second.dateEntree);

	// On indique que la place a ete remplie dans la memoire partagee
	memStart(sharedMemId, semTabId, &voituresGarees);
	voituresGarees->tab[place-1] = voiturier_it->second;
	memStop(semTabId, &voituresGarees);

	// On enleve le voiturier qui vient de mourir de la structure de donnee
	voituriers.erase(voiturier_it);
}

static void init()  {

	// On arme les signaux
	setHandler(SIGCHLD, finVoiturier);
	setHandler(SIGUSR2, destruction);

	// Ouverture du canal de la porte
	canalId = open(canalName, O_RDONLY);
}

static void moteur() {
	Voiture voiture;

	// Si on lit quelque chose
	if(read(canalId, &voiture, sizeof(Voiture)) > 0) {

		// On dessine la voiture au niveau de la barriere
		DessinerVoitureBarriere(typeEntree, voiture.conducteur);

		//On verifie si il y a une place libre
		unsigned int nbPlaces;
		memStart(sharedMemId, semTabId, &voituresGarees);
		nbPlaces = voituresGarees->nbPlaces;
		memStop(semTabId, &voituresGarees);

		// Pas de place libre
		if(nbPlaces == 0)  {

			// On affiche une requete
			AfficherRequete(typeEntree, voiture.conducteur, voiture.dateEntree);

			// On ajoute la requete a la memoire partagee
			memStart(sharedMemId, semTabId, &voituresGarees);
			voituresGarees->requetes[typeEntree-1] = voiture;
			memStop(semTabId, &voituresGarees);

			// On baisse le semaphore associe a cette porte
			struct sembuf buffer = {semType, -1, 0};
			while((semop(semTabId, &buffer, 1) == -1) && (errno == EINTR));

			// Le semaphore a ete leve par un autre processus, on peut faire rentrer la voiture
			// On efface la requete
			Effacer(zoneAffichage);
		}

		// On demande a garer la voiture
		memStart(sharedMemId, semTabId, &voituresGarees);
		voituresGarees->nbPlaces--;
		memStop(semTabId, &voituresGarees);

		// On gare la voiture et recupere le pid du voiturier
		pid_t voiturierPid = GarerVoiture(typeEntree);

		// On insere le couple (voiturier, voiture) dans notre structure.
		voituriers[voiturierPid] = voiture;

		// On attend parce qu'on est pas suisses.
		sleep(TEMPO);
	}


}

static void destruction(int signal) {
	// On ignore le signal SIGCHLD (qui sera declenche a la fin de la destruction d'un voiturier
	setHandler(SIGCHLD, SIG_IGN);
	// On demande gentillement la destruction de tous les voituriers en cours
	for(auto it = voituriers.begin(); it != voituriers.end(); it++)  {
		kill(it->first, SIGUSR2);
	}
	// On attent leur destruction
	for(auto it = voituriers.begin(); it != voituriers.end(); it++)  {
		waitpid(it->first, NULL, 0);
	}

	// On ferme le canal associe a l'entree
	close(canalId);

	// Fin de la tache
	exit(0);
}

void Entree(TypeBarriere barriere, int memId, int semId)
// Initialisation de l'entree, puis execution en boucle de moteur
//
{
	// On initialise les attributs de la porte en fonction des parametres
	typeEntree = barriere;
	if(barriere == PROF_BLAISE_PASCAL) {
		canalName = "CANAL_PROF_BP";
		semType = SEM_P1;
		zoneAffichage = REQUETE_R1;
	}
	else if(barriere == AUTRE_BLAISE_PASCAL) {
		canalName = "CANAL_AUTRE_BP";
		semType = SEM_P2;
		zoneAffichage = REQUETE_R2;
	}
	else if(barriere == ENTREE_GASTON_BERGER) {
		canalName = "CANAL_GB";
		semType = SEM_P3;
		zoneAffichage = REQUETE_R3;
	}
	sharedMemId = memId;
	semTabId = semId;

	init();
	for (;;) {
		moteur();
	}
} //----- fin de Entree

