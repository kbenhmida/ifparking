/*************************************************************************
                           Entree  -  tache Entree
                             -------------------
    début                : Entree
    copyright            : (C) Entree par B3430
    e-mail               : Entree
*************************************************************************/

//---------- Interface de la tache <Entree> (fichier Entree.h) ---------
#if ! defined ( Entree_H )
#define Entree_H

//------------------------------------------------------------------------
// Rôle du module <Entree>
//
//
//------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////  INCLUDE
//--------------------------------------------------- Interfaces utilisées
#include <stdlib.h>
#include "/shares/public/tp/tp-multitache/Outils.h"
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
void Entree(TypeBarriere barriere, int memId, int semId);
// Mode d'emploi :
// Procedure de la tache Entree
// Contrat :
// Accepte les TypeBarriere suivants : PROF_BLAISE_PASCAL, AUTRE_BLAISE_PASCAL
// et ENTREE_GASTON_BERGER. Toute autre porte aura un comportement non garanti.

#endif // Entree_H

