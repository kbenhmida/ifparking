COMP = g++
EDL = g++
RM = rm 
EXE = Parking
CLEAN = efface
CPPFLAGS = -std=c++11 -c -Wall
RMFLAGS = -f
LIBSPATH = -L /shares/public/tp/tp-multitache/
LIBS = -l tp -l ncurses -l tcl
INTERFACE = Mere.h Clavier.h Entree.h Sortie.h Config.h
REAL = $(INTERFACE:.h=.cpp)
OBJ = $(INTERFACE:.h=.o)

.PHONY : $(CLEAN)

$(EXE) : $(OBJ)
	$(EDL) -std=c++11 -o $(EXE) $(OBJ) $(LIBSPATH) $(LIBS)

%.o : %.cpp
	$(COMP) $(CPPFLAGS) $<

$(CLEAN) :
	$(RM) $(RMFLAGS) *.o $(EXE) core
