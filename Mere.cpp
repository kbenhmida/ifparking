/*************************************************************************
                           Mere  -  Cette tache sert ainitialiser
                           l'application multitache
                             -------------------
    début                : Mere
    copyright            : (C) Mere par Mere
    e-mail               : Mere
*************************************************************************/

//---------- Réalisation de la tâche <Mere> (fichier Mere.cpp) ---

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <unistd.h>
#include <algorithm>
//------------------------------------------------------ Include personnel
#include "Mere.h"
#include "Clavier.h"
#include "Entree.h"
#include "Sortie.h"
#include "/shares/public/tp/tp-multitache/Heure.h"
#include "Config.h"

///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
int main(void)
// Sequence d'initialisation de l'application multitache : creation des objets passifs + taches
// Sequence de destruction : restitution des ressources.
{
/* ---------- PHASE INITIALISATION ---------- */
	pid_t noClavier;
	pid_t noEntree1;
	pid_t noEntree2;
	pid_t noEntree3;
	pid_t noSortie;
	pid_t noHeure;

	//On masque SIGUSR2 (voir Config.h et Config.cpp pour plus d'infos sur la procedure setHandler)
	setHandler(SIGUSR2, SIG_IGN);

	//Creation des canaux de communication entre Clavier et Entree
	mkfifo("CANAL_PROF_BP", 0660);
	mkfifo("CANAL_AUTRE_BP", 0660);
	mkfifo("CANAL_GB", 0660);
	mkfifo("CANAL_SORTIE", 0660);

	//Creation de la mémoire partagée
	int memId = shmget(KEY, sizeof(VoituresGarees), (IPC_CREAT|IPC_EXCL|PERMISSIONS));

	//Creation du tableau de semaphores
	int semId = semget(KEY, NB_SEMAPHORES, (IPC_CREAT|IPC_EXCL|PERMISSIONS));
	semctl(semId, SEM_P1, SETVAL, 0); //Semaphore porte 1
	semctl(semId, SEM_P2, SETVAL, 0); //Semaphore porte 2
	semctl(semId, SEM_P3, SETVAL, 0); //Semaphore porte 3
	semctl(semId, SEM_SHARED_MEM, SETVAL, 1); //Semaphore mutex memoire partagee

	//Initialisation de la mémoire partagée et de l etat initial du parking
	Voiture voitureVide = {AUCUN, 0, 0};
	VoituresGarees * voituresGarees = (VoituresGarees *)shmat ( memId, NULL, 0 );
	fill_n(voituresGarees->tab, NB_PLACES_PARKING, voitureVide);
	fill_n(voituresGarees->requetes, NB_PORTES_ENTREE, voitureVide);
	voituresGarees->nbPlaces = NB_PLACES_PARKING;
	shmdt (voituresGarees);

	InitialiserApplication(TERMINAL);
	
	noHeure = ActiverHeure();
	
	// Clonage pour la creation des taches
	if((noClavier=fork()) == 0)
	{
		Clavier();
	}
	else if((noEntree1=fork()) == 0)
	{
		Entree(PROF_BLAISE_PASCAL, memId, semId);
	}
	else if((noEntree2=fork()) == 0)
	{
		Entree(AUTRE_BLAISE_PASCAL, memId, semId);
	}
	else if((noEntree3=fork()) == 0)
	{
		Entree(ENTREE_GASTON_BERGER, memId, semId);
	}
	else if((noSortie=fork()) == 0)
	{
		Sortie(memId, semId);
	}
	else
	{
		// Attente de la fin de la tache clavier
		waitpid(noClavier, NULL, 0);

		/* ---------- PHASE DESTRUCTION ---------- */

		// Envoi du signal SIGUSR2 pour demander la fin des processus
		kill(noSortie, SIGUSR2);
		kill(noEntree3, SIGUSR2);
		kill(noEntree2, SIGUSR2);
		kill(noEntree1, SIGUSR2);
		kill(noHeure, SIGUSR2);

		//Attente de la fin des processus
		waitpid(noSortie, NULL, 0);
		waitpid(noEntree3, NULL, 0);
		waitpid(noEntree2, NULL, 0);
		waitpid(noEntree1, NULL, 0);
		waitpid(noHeure, NULL, 0);

		//Suppression de la mémoire partagée
		shmctl(memId, IPC_RMID, 0);

		//Suppression du tableau de semaphores
		semctl(semId, 0, IPC_RMID, 0);

		//Suppression des canaux
		unlink("CANAL_SORTIE");
		unlink("CANAL_GB");
		unlink("CANAL_AUTRE_BP");
		unlink("CANAL_PROF_BP");

		TerminerApplication(true);
		exit(0);
	}
	
	return 0;
} //----- fin de Mere

