/*************************************************************************
                           Config
                             -------------------
    début                : Config
    copyright            : (C) Config par Config
    e-mail               : Config
*************************************************************************/

//---------- Réalisation du module <Config> (fichier Config.cpp) -----

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <sys/sem.h>
#include <sys/shm.h>
#include <errno.h>
#include <iostream>
//------------------------------------------------------ Include personnel
#include "Config.h"

///////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques
static struct sembuf buffer;
//------------------------------------------------------ Fonctions privées


//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques
void setHandler(int numSignal, void (*fonction) (int))
{
	struct sigaction action;

	action.sa_handler = fonction;
	action.sa_flags = 0;
	sigaction(numSignal, &action, NULL);
}

void memStart(int sharedMemId, int semTabId, VoituresGarees** voituresGarees)
{
	buffer = {SEM_SHARED_MEM, -1, 0};
	while((semop(semTabId, &buffer, 1) == -1) && (errno == EINTR));
	*voituresGarees = (VoituresGarees *)shmat(sharedMemId, NULL, 0 );
}

void memStop(int semTabId, VoituresGarees** voituresGarees)
{
	shmdt(*voituresGarees);
	buffer = {SEM_SHARED_MEM, 1, 0};
	semop(semTabId, &buffer, 1);
}
